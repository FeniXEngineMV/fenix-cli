![fenix-wizard-logo](docs/.vuepress/public/img/logos/fenixwizard-logo.png)

## @fenixengine/wizard
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)[![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

<a href='https://ko-fi.com/ltngames' target='_blank'><img height='36' style='border:0px;height:36px;' src='https://az743702.vo.msecnd.net/cdn/kofi2.png?v=0' border='0' alt='Buy Me a Coffee at ko-fi.com' /></a>

>> A lightweight CLI for use with FeniXEngine plugins and tools. `FeniXWizard` will help you get started with developing plugins for FeniXEngine MV, it does this by combining necessary development tools. These tools help with the bundling of all plugins and serving files to a server for easy testing and streamlined productivity.

* Website: https://fenixenginemv.gitlab.io/fenix-wizard
* Guide: https://fenixenginemv.gitlab.io/fenix-wizard/guide/
* CLI Options: https://fenixenginemv.gitlab.io/fenix-wizard/cli-options/

![fenix-wizard-init](docs/.vuepress/public/img/fenixwizard-init.gif)

## Features

* Watch for changes to files
* Serve an RPG Maker MV game project
  * Live reload
* Next-Gen JavaScript bundler
* Auto ESLint configuration
* Auto install node packages
* Dynamically writes to Parameters.js

## Roadmap

* Babel support
* Auto configuration for XO and JSHint linters
* nwjs live reload support
* Live package install
* Frontend GUI

## Credits

 While this is open source and credit is not required, it would be appreciated if you linked to FeniX Engine's website or GitLab repository to show support.

https://fenixenginemv.gitlab.io/

## Contributing
Please read over the [Contribution Guide](https://gitlab.com/FeniXEngineMV/wizard/blob/master/CONTRIBUTING.md)

## License
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
[The MIT License](https://gitlab.com/FeniXEngineMV/wizard/blob/master/LICENSE.md)