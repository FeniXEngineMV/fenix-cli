import test from 'ava'

import { getPluginTag, filterText, logger, getParameters } from '../../src/utils/'

const srcDir = `${global.FIX_DIR}/plugins/`
const pluginPath = `${srcDir}/fenix-core/`

test.before(t => {
  logger.setOptions({ silent: true })
})

test('resolves and throws', async t => {
  await t.notThrowsAsync(getPluginTag(pluginPath, '@plugindesc'))
  await t.throwsAsync(getPluginTag(srcDir, '@plugindesc'))
})

test('regular expression returns correct matches', async t => {
  const pattern = new RegExp(`(@${'pluginname'})([^\\r\\n]*)`, 'g')
  filterText(await getParameters(pluginPath), pattern, (matches) => {
    t.deepEqual(matches[0], '@pluginname FeniXCore')
    t.deepEqual(matches[2], ' FeniXCore')
  })
})

test('finds tag and returns value', async t => {
  const name = await getPluginTag(pluginPath, 'pluginname')
  const module = await getPluginTag(pluginPath, 'modulename')
  const author = await getPluginTag(pluginPath, 'author')
  t.deepEqual(name, 'FeniXCore')
  t.deepEqual(author, 'FeniXEngine Contributors')
  t.deepEqual(module, 'FeniX')
})

test('returns error when tag does not exist', t => {
  return getPluginTag(pluginPath, 'external').then((result) => {
    t.is(result instanceof Error, true, 'Should be an Error')
    t.is(result.message.includes('@external'), true, 'Error should include missing tag in message')
  })
})
