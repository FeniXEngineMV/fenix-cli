import test from 'ava'
import fs from 'fs-extra'

import { install, createPackageJson } from '../../src/utils'

import { logger } from '../../src/utils/logger'

test.before(t => {
  logger.setOptions({ silent: true })
})

test.after(async t => {
  await global.cleanAllTemp()
})

test('Installs magnet-uri and saves as dependency', async t => {
  const path = global.newTempDir()
  // Create an empty package.json before running test
  await createPackageJson({ destination: path })
  await install(['magnet-uri'], { saveDev: false, path })
  const packageData = await fs.readJson(`${path}/package.json`)

  t.is(typeof packageData, 'object', 'package.json should exists as an object')
  t.is(typeof packageData.dependencies['magnet-uri'], 'string', 'magnet-uri should exist as a dependency')

  const packageExists = await fs.exists(`${path}/node_modules/magnet-uri/`)
  t.is(packageExists, true, 'magnet-uri should exist in node_modules')
})
