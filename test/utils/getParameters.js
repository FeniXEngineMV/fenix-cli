import test from 'ava'

import { getParameters } from '../../src/utils'

import { logger } from '../../src/utils/logger'

test.before(t => {
  logger.setOptions({ silent: true })
})

test.after(async t => {
  await global.cleanAllTemp()
})

test('resolves and throws', async t => {
  const plugin = `${global.FIX_DIR}/plugins/fenix-core/`
  await t.notThrowsAsync(getParameters(plugin), 'Resolves when parameters exists')
  await t.throwsAsync(getParameters(`${plugin}/non-existence/`), null, 'throws when no parameters exist')
})
