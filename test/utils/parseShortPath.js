import test from 'ava'

import { parseShortPath } from '../../src/utils'

test('parses a full path to dir/file.js', async t => {
  const path = `${global.FIX_DIR}/plugins/fenix-core/main.js`
  t.is(parseShortPath(path), 'fenix-core/main.js')
})
