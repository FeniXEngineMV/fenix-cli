
import test from 'ava'
import { Logger, logger } from '../../src/utils'

const stdout = require('test-console').stdout
const arrayIncludes = (arr, str) => arr.some(el => el.includes(str))

test.before(t => {
  logger.setOptions({ silent: true })
})

test('returns an object', async t => {
  t.is(typeof logger === 'object', true, 'logger instance should be an object')
})

test('logs messages, warnings and errors', async t => {
  const logger = Logger()
  const output = stdout.inspectSync(() => {
    logger.log('a message')
    logger.warn('a warning')
    logger.error('an error')
  })
  t.true(output.length > 0)

  t.is(arrayIncludes(output, 'a message'), true)
  t.is(arrayIncludes(output, 'a warning'), true)
  t.is(arrayIncludes(output, 'an error'), true)
})

test('respects silent option and does not log', t => {
  const logger = Logger()
  logger.setOptions({ silent: true })
  const output = stdout.inspectSync(() => {
    logger.log('a message')
    logger.warn('a warning')
    logger.error('an error')
  })
  t.deepEqual(output, [], 'should return empty array(console) as nothing should be logged')
})
