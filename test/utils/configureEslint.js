import test from 'ava'
import fs from 'fs-extra'

import { configureEslint, createPackageJson } from '../../src/utils'

import { logger } from '../../src/utils/logger'

test.before(t => {
  logger.setOptions({ silent: true })
})

test.after(async t => {
  await global.cleanAllTemp()
})

test('properly writes correct config to package.json', async t => {
  const output = global.newTempDir()
  await createPackageJson({ destination: output })
  await configureEslint(output, ['eslint-plugin-rpgmaker', 'standard'])
  const packageData = await fs.readJson(`${output}/package.json`)

  t.is(typeof packageData.eslintConfig, 'object', 'eslint should exist as an object in package.json')
  t.is(packageData.eslintConfig.extends, 'standard', 'eslint should extend standard')
  t.deepEqual(packageData.eslintConfig.plugins, ['rpgmaker'], 'eslint should have the rpgmaker plugin')
})
