import test from 'ava'
import fs from 'fs'
import eol from 'eol'

import { fileChangeWatcher, startWatching } from '../src/watcher'
import { logger } from '../src/utils/logger'

const fsp = fs.promises
const TARGET_DIR = `${global.FIX_DIR}/plugins/watcher/`
const stdout = require('test-console').stdout
const arrayIncludes = (arr, str) => arr.some(el => el.includes(str))
const ORIGINAL_DATA = {
  core: null,
  empty: null
}

function timeout (ms) {
  return new Promise(resolve => setTimeout(resolve, ms))
}

test.before(async t => {
  logger.setOptions({ silent: true })
  ORIGINAL_DATA.core = await fsp.readFile(`${TARGET_DIR}/fenix-core/main.js`)
  ORIGINAL_DATA.empty = await fsp.readFile(`${TARGET_DIR}/fenix-plugin/main.js`)
})

test.after.always(async t => {
  await global.cleanAllTemp()
  await fsp.writeFile(`${TARGET_DIR}/fenix-core/main.js`, ORIGINAL_DATA.core)
  await fsp.writeFile(`${TARGET_DIR}/fenix-plugin/main.js`, ORIGINAL_DATA.empty)
})

test('fileChangeWatcher is a function', async t => {
  t.is(typeof fileChangeWatcher, 'function', 'should return as function')
})

test('fileChangeWatcher logs message on ready', async t => {
  logger.setOptions({ silent: false })
  const inspect = stdout.inspect()
  fileChangeWatcher({ target: `${TARGET_DIR}/fenix-core/` })
  await timeout(1800)
  t.is(arrayIncludes(inspect.output, 'Waiting for changes...'), true)
  inspect.restore()
  logger.setOptions({ silent: true })
})

test.cb('fileChangeWatcher listens to changes', t => {
  t.plan(1)
  const path = `${TARGET_DIR}/fenix-core/`

  fileChangeWatcher({ target: path }, () => {
    t.pass()
    t.end()
  })

  setTimeout(() => {
    fs.writeFile(`${path}/main.js`, '//some random characters', (err) => {
      if (err) { t.fail(err) }
    })
  }, 1800)
})

test('startWatching is a function', t => {
  t.is(typeof startWatching, 'function', 'should return as function')
})

test('startWatching builds from source after changes', async t => {
  t.plan(1)

  const path = `${TARGET_DIR}/fenix-plugin/`
  const destination = global.newTempDir()

  startWatching({ target: path, destination })

  // Fire the watcher's on change event
  await timeout(500)
  await fsp.writeFile(`${path}/main.js`, '//testing bundle')
  await timeout(3000)
  // Read data that should be built after the bundler is fired
  const fileDataAfterWrite = await fsp.readFile(`${destination}/FeniXPlugin.js`, 'utf8')
  t.snapshot(eol.lf(fileDataAfterWrite))
})
