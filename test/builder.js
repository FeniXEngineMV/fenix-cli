import test from 'ava'
import fs from 'fs-extra'
import eol from 'eol'

import { builder } from '../src/builder'
import { PluginRollup } from '../src/rollup'
import { logger } from '../src/utils/logger'
import fenixCLI from '../bin/fenix-cli'

const target = `${global.FIX_DIR}/plugins/`

test.before(t => {
  logger.setOptions({ silent: true })
})

test.after(async t => {
  await global.cleanAllTemp()
})

test('builder is exposed to API', async t => {
  t.is(typeof fenixCLI, 'object')
  t.is(typeof fenixCLI.builder, 'function')
})

test('bundles all plugins from a multi plugin project', async t => {
  const destination = global.newTempDir()

  await builder({ target, destination })
  const files = await fs.readdir(destination)
  t.deepEqual(files, ['FeniXCore.js', 'FeniXPlugin.js'])
})

test('bundles a single plugin from a multi plugin project', async t => {
  const destination = global.newTempDir()
  await builder({ target: `${target}/fenix-core/`, destination })

  const files = await fs.readdir(destination)
  t.deepEqual(files, ['FeniXCore.js'])
})

test('output of plugin should contain all modules bundled', async t => {
  const destination = global.newTempDir()
  await builder({ target: `${target}/fenix-core/`, destination })

  const file = await fs.readFile(`${destination}/FeniXCore.js`, 'utf8')
  t.snapshot(eol.lf(file))
})

test('accepts a bundler plugin', async t => {
  const destination = global.newTempDir()
  await builder({
    target: `${target}/fenix-core/main.js`,
    bundler: PluginRollup,
    destination
  })
  const files = await fs.readdir(`${destination}/`, 'utf8')
  t.deepEqual(files, ['FeniXCore.js'])
})

test('generates a sourcemap', async t => {
  const destination = global.newTempDir()
  await builder({
    target: `${target}/fenix-core/main.js`,
    bundler: PluginRollup,
    sourcemap: true,
    destination
  })

  const file = await fs.readFile(`${destination}/FeniXCore.js`, 'utf8')
  t.snapshot(eol.lf(file))
})
