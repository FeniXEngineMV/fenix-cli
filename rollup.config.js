import resolve from 'rollup-plugin-node-resolve'
import eslint from 'rollup-plugin-eslint-bundle'

export default {
  input: `./src/main.js`,
  external: [
    'fs-extra',
    'http',
    'https',
    'path',
    'tracer',
    'ora',
    'inquirer',
    'rollup',
    'browser-sync',
    'rollup-plugin-eslint-bundle',
    'rollup-plugin-node-resolve',
    'chalk',
    'adm-zip'
  ],
  output: [{
    file: `./bin/fenix-wizard.js`,
    format: 'cjs',
    indent: false,
    banner: '#!/usr/bin/env node',
    sourcemap: process.env.NODE_ENV === 'test'
  }
  ],
  plugins: [
    resolve({
      jsnext: true,
      module: true
    }
    ),
    eslint({
      useEslintrc: true,
      fix: true
    })
  ]
}
