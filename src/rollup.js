import { rollup } from 'rollup'
import nodeResolve from 'rollup-plugin-node-resolve'

export function PluginRollup () {
  return {
    async bundle (options) {
      try {
        const rollupOptions = {
          input: options.target,
          external: ['fs', 'http', 'https', 'path', 'PIXI'].concat(options.external),
          output: {
            file: `${options.destination}/${options.filename}`,
            format: 'iife',
            name: options.modulename,
            banner: options.parameters,
            footer: options.footer,
            sourcemap: options.sourcemap,
            indent: false
          },

          onwarn (warning) {
            options.logger.warn(warning.message)
          },

          plugins: [
            nodeResolve({
              module: true,
              preferBuiltins: false
            })
          ]
        }

        const bundler = await rollup(rollupOptions)
        await bundler.write(rollupOptions.output)
      } catch (error) {
        throw error
      }
    }
  }
}
