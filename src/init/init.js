import inquirer from 'inquirer'
import { buildProjectBase } from './buildProjectBase'
import { logger } from '../utils'

const chalk = require('chalk')

export function startInit (options) {
  logger.log(`
  ${chalk.blue('Welcome to FeniXWizard!')}
  FeniXWizard is a developer tool for creating an environment that makes developing
  RPG Maker MV plugins faster and easier.

  ${chalk.green('Follow the on-screen instructions to get started!.')}
  `)

  inquirer.prompt(
    [
      {
        type: 'input',
        name: 'destination',
        message: 'Where would you like to create the project',
        default: './'
      },
      {
        type: 'input',
        name: 'projectName',
        message: 'Project name',
        default: 'generated-project'
      },
      {
        type: 'input',
        name: 'projectDescription',
        default: '',
        message: 'Project description'
      },
      {
        type: 'input',
        name: 'projectAuthor',
        default: 'FeniXEngine Contributers',
        message: 'Author name'
      },
      {
        type: 'list',
        name: 'linter',
        default: 0,
        message: 'Choose a linter',
        choices: [
          {
            value: 'eslint',
            name: 'ESLint'
          },
          {
            value: 'xo',
            name: 'XO'
          },
          {
            value: 'jshint',
            name: 'JSHint'
          },
          {
            value: 'none',
            name: 'none'
          }
        ]
      },
      {
        type: 'checkbox',
        name: 'eslintPlugins',
        default: [1, 2],
        message: 'Which ESLint plugins would you like to install',
        choices: [
          {
            value: 'eslint-plugin-rpgmaker',
            name: 'RPG Maker MV Global Variables'
          },
          {
            value: 'standard',
            name: 'StandardJS Config'
          },
          {
            value: 'semistandard',
            name: 'StandardJS Config w/ Semicolons'
          },
          {
            value: 'airbnb',
            name: 'Airbnb Style'
          }
        ],
        when: (answers) => answers.linter === 'eslint'
      },
      {
        type: 'checkbox',
        name: 'optionalPackages',
        default: [1],
        message: 'Would you like to install other useful packages',
        choices: [
          {
            value: 'fenix-tools',
            name: 'FeniXTools - A modular RPG Maker MV plugin library'
          },
          {
            value: 'ava',
            name: 'AVA - Futuristic test runner'
          },
          {
            value: 'flow-bin',
            name: 'Flow - A static type checker for JavaScript'
          }
        ]
      },
      {
        type: 'confirm',
        name: 'initializeGit',
        default: true,
        message: 'Would you like to initialize git for this project?'
      },
      {
        type: 'confirm',
        default: false,
        name: 'downloadDemo',
        message: 'Download FeniX Lightweight MV Demo'
      }
    ])
    .then(answers => buildProjectBase(answers))
}
