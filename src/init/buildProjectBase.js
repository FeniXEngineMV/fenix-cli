import fs from 'fs-extra'
import { downloadFile } from '../utils/downloadFile'
import AdmZip from 'adm-zip'

import {
  createPackageJson,
  logger,
  install,
  configureEslint,
  initializeGit
} from './../utils/'

import config from '../config'

const CURR_DIR = process.cwd()
const GAME_FILENAME = 'fenix-lightweight-mv-game-v0.0.1.zip'
const GAME_LINK = `https://gitlab.com/FeniXEngineMV/fenix-lightweight-mv-game/-/archive/v0.0.1/${GAME_FILENAME}`

async function copyTemplateFiles (destination) {
  const templateDir = `${config.rootDir}/templates/base/`

  try {
    const files = await fs.readdir(templateDir)
    await fs.ensureDir(`${destination}/src/`)

    for (const file of files) {
      const fileDir = `${templateDir}/${file}`
      const fileDestDir = `${destination}/src/${file}`
      const fileExists = await fs.exists(fileDestDir)

      if (fileExists === false) {
        await fs.copyFile(fileDir, fileDestDir)
      }
    }
  } catch (error) {
    Promise.reject(logger.error(error))
  }
}

async function injectParamterTags (options) {
  try {
    const { author, pluginname, destination, description } = options
    const parameterData = await fs.readFile(`${destination}/src/Parameters.js`, 'utf8')
    const coreData = await fs.readFile(`${destination}/src/Core.js`, 'utf8')
    const authorTag = `@author ${author}`
    const pluginnameTag = `@pluginname ${pluginname}`
    const descriptionTag = `@plugindesc ${description}`

    const newParameters = parameterData
      .replace(`@author`, `${authorTag}`)
      .replace(`@pluginname`, `${pluginnameTag}`)
      .replace(`@plugindesc`, `${descriptionTag}`)

    const newCoreData = coreData
      .replace('<X_PluginName>', `<${pluginname}>`)

    await fs.writeFile(`${destination}/src/Parameters.js`, newParameters)
    await fs.writeFile(`${destination}/src/Core.js`, newCoreData)
  } catch (error) {
    throw new Error(logger.error(error))
  }
}

async function installLinterPackages (options) {
  if (options.linter === 'none') {
    return
  }
  const plugins = options.eslintPlugins
  const destination = `${CURR_DIR}/${options.destination}`
  try {
    await install([options.linter], { path: destination })

    for (const plugin of plugins) {
      if (plugin === 'standard') {
        await install(['eslint-config-standard'], { peers: true, path: destination })
      } else if (plugin === 'semistandard') {
        await install(['eslint-config-semistandard'], { peers: true, path: destination })
      } else if (plugin === 'airbnb') {
        await install(['airbnb'], { peers: true, path: destination })
      } else {
        await install([plugin], { path: destination })
      }
    }
    await configureEslint(destination, plugins)
  } catch (error) {
    Promise.reject(logger.error(error))
  }
}

async function downloadAndExtract (path) {
  logger.spinner.start('Downloading FeniX Lightweight MV demo')
  const zipDirName = GAME_FILENAME.replace('.zip', '')
  const demoData = await downloadFile({ url: GAME_LINK })
  // Extract downloaded zip
  const zip = new AdmZip(demoData)
  zip.extractAllTo(`${path}`, true)

  await fs.rename(`${path}/${zipDirName}`, `${path}/game/`)
  logger.spinner.succeed('Download complete')
}

export async function buildProjectBase (options) {
  try {
    const destination = `${CURR_DIR}/${options.destination}`

    await fs.ensureDir(`${destination}/src`)
    await createPackageJson({
      name: options.projectName,
      description: options.projectDescription,
      author: options.projectAuthor,
      destination
    })
    await copyTemplateFiles(destination)
    await injectParamterTags({
      author: options.projectAuthor,
      description: options.projectDescription,
      pluginname: options.projectName,
      destination
    })
    await installLinterPackages(options)

    if (options.optionalPackages.length > 0) {
      await install(options.optionalPackages, { path: destination })
    }

    if (options.initializeGit) {
      initializeGit({ path: destination })
    }

    if (options.downloadDemo) {
      await downloadAndExtract(destination)
    } else {
      await fs.ensureDir(`${destination}/game`)
    }
  } catch (error) {
    Promise.reject(logger.error(error))
  }
}
