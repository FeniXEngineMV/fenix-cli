import {
  serverCommand,
  buildCommand,
  initCommand
} from './commands/index'

import config from './config'
import { startInit } from './init'
import { builder } from './builder'

function _loadCommands (program) {
  const commands = [serverCommand, buildCommand, initCommand]
  commands.forEach(command => command(program))
}

const program = require('commander')
_loadCommands(program)

program
  .version(config.version)
  .arguments('<command> [options]')
  .usage('<command> [options]')
  .parse(process.argv)

if (program.args.length <= 0 && require.main === false) {
  startInit()
}

// Expose to API
export default { builder }
