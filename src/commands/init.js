import { startInit } from './../init/index'

export function initCommand (program) {
  program
    .command('init')
    .description('A guided setup for the setup of a plugin project')
    .action(startInit)
}
