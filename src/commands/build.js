import { builder } from './../builder'
import { startWatching } from './../watcher'

export function buildCommand (program) {
  program
    .command('build')
    .description(`Builds a plugin from it's source files`)
    .option('-t, --target <path>', 'the target directory where all plugins are located. defaults to ./src/')
    .option('-d, --destination <path>', 'the path to save file after the build. defaults to ./game/js/plugins')
    .option('-w, --watch', 'Watches files to rebuild when changes occur')
    .option('-s, --sourcemap', 'choose to generate a sourcemap inline with the plugin')
    .action((options) => {
      if (options.watch) {
        startWatching(options)
      }
      builder(options)
    })
}
