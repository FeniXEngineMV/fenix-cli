import { startServer } from './../server'

export function serverCommand (program) {
  program
    .command('serve')
    .description('Starts a server and syncs all changes')
    .option('-t, --target <path>', 'the target directory to build plugins from. defaults to ./src/ and builds to [destination] /js/plugins/')
    .option('-d, --destination <path>', 'directory to serve from. defaults to ./game/')
    .option('-p, --port [number]', 'Choose which port to run the server on')
    .action(startServer)
}
