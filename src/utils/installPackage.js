import { logger } from './logger'
import { pipeSpawn } from './pipeSpawn'

export async function install (modules, options = {}) {
  const { saveDev = true, path = process.cwd(), peers = false } = options
  const command = peers ? `npx` : 'npm'
  const subCommand = peers ? 'install-peerdeps' : 'install'
  const args = []

  args.push(subCommand, ...modules)

  if (saveDev) {
    args.push(peers ? '-dev' : '-D')
  }

  try {
    logger.spinner.start(`Installing package(s) ${modules}`)
    await pipeSpawn(command, args, {
      cwd: path
    })
    logger.spinner.succeed(`Success installing package(s) ${modules}`)
  } catch (error) {
    logger.spinner.fail(`Failed installing package(s) ${modules}`)
    Promise.reject(logger.error(`Failed to install modules ${modules}`))
  }
}
