import ora from 'ora'
import chalk from 'chalk'

import { parseShortPath } from './parseShortPath'
import config from '../config'

const readline = require('readline')
const timestamp = require('time-stamp')
const PrettyError = require('pretty-error')
const cliCursor = require('cli-cursor')

export function Logger (options = {}) {
  const _options = Object.assign(options, {
    silent: false,
    logLevel: 4,
    timestamp: true,
    useSpinner: true
  })
  let _currentLine = 0
  const _savedLines = []
  const _currentTime = () => timestamp('[HH:mm:ss]')
  const prettyError = new PrettyError()

  const _spinner = ora({
    enabled: config.isTest || !_options.useSpinner ? false : undefined,
    stream: process.stdout
  })

  const _timeLog = () => chalk.blue(_currentTime())
  const _pathLog = (filename, path) => chalk.green(`${filename} -> ${parseShortPath(path)}`)
  const _serverBadgeLog = (target, port) => {
    return `
  ===============================
  ${chalk.blue.bold('FeniXWizard')} v${config.version}\n
  Serving => ${parseShortPath(target)}\n
  ${chalk.bold('Game')} => ${chalk.blue.bold(`http://localhost:${port}`)}
  ===============================
  `
  }
  const _buildMessage = (message, filename, path) => {
    return `${_timeLog()} ${message} ${_pathLog(filename, path)}`
  }

  function _saveLine () {
    _savedLines.push(_currentLine)
    return _currentLine
  }

  function _clearSavedLines () {
    _savedLines.forEach(line => {
      _clearLine(line)
      _savedLines.splice(_savedLines.indexOf(line), 1)
    })
  }

  function _clearLine (line) {
    if (line) {
      readline.moveCursor(process.stdout, 0, -1, () => {
        readline.clearScreenDown(process.stdout, () => {
          _currentLine--
        })
      })
    }
  }

  function _write (message) {
    if (_options.silent) {
      return message
    }
    if (_spinner.isSpinning) {
      _spinner.clear()
    }
    _clearSavedLines()
    _currentLine++
    process.stdout.write(`${message}\n`)
    return message
  }

  return {
    get spinner () {
      return _spinner
    },

    get chalk () {
      return chalk
    },

    saveLog (message) {
      this.log(message)
      _saveLine()
    },

    setOptions (options) {
      Object.assign(_options, options)
    },

    hideCursor () {
      cliCursor.hide()
    },

    log (message) {
      if (_options.logLevel > 3) {
        return _write(chalk.bold(message))
      }
    },

    logServerBadge (target, port) {
      _write(_serverBadgeLog(target, port))
    },

    logBuild (filename, path, type = 'succeed') {
      const failedMessage = _buildMessage('Failed', filename, path)
      const succeedMessage = _buildMessage('Bundled', filename, path)

      switch (type) {
        case 'succeed':
          return _write(succeedMessage)
        case 'failed':
          _write(failedMessage)
      }
    },

    warn (message, badge = true) {
      const badgeMessage = badge ? 'Warning' : ''
      const styledMessage = `${chalk.black.bgYellow(badgeMessage)} ${chalk.yellow(message)}`
      return _write(styledMessage)
    },

    error (message) {
      if (_options.logLevel > 1) {
        return _write(`\n${_timeLog()} ${prettyError.render(message)}`)
      }
    }
  }
}

export const logger = Logger()
