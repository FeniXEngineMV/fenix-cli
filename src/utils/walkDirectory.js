import fs from 'fs-extra'
import path from 'path'

async function fileType (path) {
  const item = await fs.stat(path)
  return item.isDirectory() ? 'directory' : item.isFile() ? 'file' : null
}

/**
 * A lightweight directory walk, for traversing directories
 *
 * @param {string} input - The input path
 * @param {function} callback - The callback to run for each item
 */
export async function walkDirectory (input, callback = () => { }) {
  const _paths = []

  const _walkDirectory = async function (input, callback) {
    const type = await fileType(input)
    try {
      switch (type) {
        case 'directory':
          const paths = await fs.readdir(input)
          for (const path of paths) {
            await _walkDirectory(`${input}/${path}`)
          }
          break
        case 'file':
          const filename = path.parse(input).name
          if (_paths.indexOf(input) > -1) {
            return
          }
          if (callback) {
            callback(input, filename)
          }
          _paths.push(input)
          break
      }
      return _paths
    } catch (error) {
      throw error
    }
  }
  return _walkDirectory(path.resolve(input), callback)
}
