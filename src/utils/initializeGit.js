import { pipeSpawn } from './pipeSpawn'

export async function initializeGit (options = {}) {
  const { path = process.cwd() } = options
  const args = []
  const command = 'git'
  const subCommand = 'init'
  args.push(subCommand)

  await pipeSpawn(command, args, {
    cwd: path
  })
}
