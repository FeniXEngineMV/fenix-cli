import { logger } from './logger'
const crossSpawn = require('cross-spawn')

export async function pipeSpawn (command, params, options) {
  const cp = crossSpawn(command, params, Object.assign({
    env: process.env
  }, options))

  cp.stdout.setEncoding('utf8').on('data', d => logger.log(`\n${d}`))
  cp.stderr.on('data', d => logger.warn(`\n${d}`, false))

  return new Promise((resolve, reject) => {
    cp.on('error', (err) => {
      reject(new Error(logger.error(err)))
    })
    cp.on('close', (code) => {
      if (code !== 0) {
        return reject(new Error(logger.error(`${command} failed`)))
      }
      return resolve()
    })
  })
}
