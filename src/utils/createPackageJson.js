import fs from 'fs-extra'

export async function createPackageJson (options) {
  const packagePath = `${options.destination}/package.json`
  try {
    const packageJsonExists = await fs.exists(packagePath)
    if (packageJsonExists) {
      return
    }
    await fs.writeJson(packagePath, {
      name: options.name,
      description: options.description,
      author: options.author,
      version: '0.1.0',
      license: 'MIT',
      repository: { private: true },
      devDependencies: {},
      dependencies: {}
    })
  } catch (error) {
    throw new Error(error)
  }
}
