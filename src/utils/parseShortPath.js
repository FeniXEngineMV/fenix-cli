import path from 'path'

export function parseShortPath (fullPath) {
  const directory = path.dirname(fullPath)
  const filename = path.basename(fullPath)
  const fileBuildDir = path.basename(directory)
  return `${fileBuildDir}/${filename}`
}
