import { filterText } from './filterText'
import { getParameters } from './getParameters'
import { logger } from './logger'

/**
 * Retrieves a tag of choice from a plugins parameters by looking for a
 * match in the plugins Parameters.js file.
 *
 * Only works for single line tags like author, plugindesc and custom tags for
 * FeniXEngine.
 *
 * @export
 * @param {string} path - The path to the plugin.
 * @param {string} tag - The tag to extract from the plugins parameters.
 * @returns {string} The value of the tag
 */
export async function getPluginTag (path, tag) {
  try {
    const pattern = new RegExp(`@(${tag})([^\\r\\n]*)`, 'g')
    const parameters = await getParameters(path)
    const match = filterText(parameters, pattern, match => match[1] === tag)[0]

    if (match) {
      return match[2].trim()
    } else {
      const message = `There was a problem finding plugin tag "@${tag}" in ${path}`
      return new Error(logger.warn(message))
    }
  } catch (error) {
    throw new Error(logger.error(error))
  }
}
