import fs from 'fs-extra'
import { logger } from './logger'

export const getParameters = async (path) => {
  try {
    const files = await fs.readdir(path)
    const file = files.filter(file => file === 'Parameters.js')[0]
    const data = await fs.readFile(`${path}/${file}`, 'utf8')
    return data
  } catch (error) {
    throw new Error(logger.error(error))
  }
}
