import path from 'path'

const isTest = process.env.NODE_ENV === 'test' || process.env.CI

const rootDir = path.dirname(__dirname)

const rootPackage = require(`${rootDir}/package.json`)

const version = rootPackage.version

export default {
  rootDir,
  rootPackage,
  isTest,
  version
}
