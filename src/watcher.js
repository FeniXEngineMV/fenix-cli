import { builder } from './builder'
import { logger } from './utils/index'

const chokidar = require('chokidar')
let watcher = null
let _onChangeCallback = null

export async function fileChangeWatcher (options, onChangeCallback) {
  if (watcher) {
    await watcher.close()
  }

  _onChangeCallback = onChangeCallback

  watcher = chokidar.watch(`${options.target}/**/*.js`, {
    persistent: true,
    ignoreInitial: true,
    awaitWriteFinish: true,
    usePolling: true
  })
    .on('ready', () => {
      setTimeout(() => {
        logger.saveLog('Waiting for changes...')
      }, 1500)
    })
    .on('change', (path, stats) => {
      if (typeof _onChangeCallback === 'function') {
        onChangeCallback(path, stats)
        setTimeout(() => {
          logger.saveLog('Waiting for changes...')
        }, 1500)
      }
    })
    .on('error', error => logger.error(`Watcher error: ${error}`))
}

async function bundleOnChange (options) {
  await builder(options)
}

export function startWatching (options) {
  const watchDir = options.target || `${process.cwd()}/src`
  const destination = options.destination || `${process.cwd()}/game/js/plugins/`
  const watchOptions = {
    target: watchDir,
    ...options
  }

  fileChangeWatcher(watchOptions, async (filepath, stats) => {
    try {
      await bundleOnChange({ ...options, watchDir, destination })
    } catch (error) {
      throw new Error(logger.error(error.message))
    }
  })
}
