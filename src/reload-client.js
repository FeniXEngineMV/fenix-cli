/* globals WebSocket */

let ReloadSocket = null
const socketServerUrl = '' // This is dynamically filled by the server

window.addEventListener('load', event => {
  if (typeof window.WebSocket === 'undefined') {
    console.error(`Browser has no WebSocket support: Cannot enable hot reload`)
  }

  ReloadSocket = new WebSocket(socketServerUrl)

  ReloadSocket.onopen = event => {
    ReloadSocket.send('reload-ready')
  }

  ReloadSocket.onmessage = message => {
    if (message.data === 'reload') {
      window.location.reload()
    }
    console.log(message.data)
  }
})

window.addEventListener('unload', event => {
  if (ReloadSocket) {
    ReloadSocket.send('browser-unload')
    ReloadSocket.send('browser-reloading')
  }
})
