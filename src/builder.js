import fs from 'fs-extra'
import path from 'path'
import { logger, walkDirectory, getParameters, getPluginTag } from './utils/index'
import { PluginRollup } from './rollup'

async function buildFromDirectory (options) {
  const files = await walkDirectory(options.target)

  for (const file of files) {
    const filename = path.basename(file)
    const dir = path.dirname(file)

    if (filename === 'main.js') {
      await build({ ...options, target: `${dir}/main.js` })
    }
  }
}

async function build (options) {
  const { target, destination, bundler = PluginRollup } = options

  try {
    const targetDirectory = path.dirname(target)
    const filename = `${await getPluginTag(targetDirectory, 'pluginname')}.js`
    const bundlerOptions = {
      modulename: await getPluginTag(targetDirectory, 'modulename'),
      parameters: await getParameters(targetDirectory),
      filename,
      logger,
      ...options
    }

    await bundler().bundle(bundlerOptions)
    logger.logBuild(filename, destination)
  } catch (error) {
    throw error
  }
}

export async function builder (options) {
  const { target = './src/', destination = './game/js/plugins/' } = options

  if (options.bundler && typeof options.bundler !== 'function') {
    logger.warn('option.bundler must be a function which returns an object')
  }

  try {
    const targetStat = await fs.stat(target)

    if (targetStat.isDirectory()) {
      await buildFromDirectory({ ...options, target, destination })
    } else if (targetStat.isFile()) {
      await build({ ...options, target, destination })
    }
  } catch (error) {
    throw error
  }
}
