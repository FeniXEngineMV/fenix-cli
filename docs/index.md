---
home: true
heroText: null
heroImage: /img/logos/fenixwizard-logo.png
actionText: Get Started →
actionLink: /guide/
features:
- title: Project Setup
  details: Generate and entire project, download an MV demo game and automatically install and configures packages
- title: Live server
  details: Start a live development server and reload when changes have been made to your plugins
- title: Next-Gen Javascript
  details: Start writing with next generation JavaScript, split code into modules and import packages, no setup required!
footer: MIT Licensed
---

It's really this easy

```bash
# Install

npm install -g @fenixengine/wizard

# Setup a new project

fenix

# Start a development server

fenix serve

```
::: warning COMPATIBILITY NOTE
FeniXCLI requires Node.js >= 8.
:::