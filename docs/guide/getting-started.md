# Getting Started

::: warning COMPATIBILITY NOTE
FeniXCLI requires Node.js >= 8. & NPM >= 5.2
:::


## Installing Node & NPM

Node is a JavaScript runtime and NPM is a package manager, both are required to use `FeniXWizard`

**Note:** _Skip to [Install `FeniXWizard`](#install-fenizwizard) if you already have NodeJS & npm installed._

### For Windows

- Download and install [NodeJS](https://nodejs.org/en/)

### For Ubuntu 18.04

**Note:** Ubuntu versions lower than `18.04` need to follow this [guide](https://nodejs.org/en/download/package-manager/)

- Install [NodeJS](https://nodejs.org/en/)

```bash
sudo apt-get install nodejs
```

- Install [npm](https://www.npmjs.com/)

```bash
sudo apt-get install npm
```

## Install `FeniXWizard` Globally

Installing `FeniXWizard` globally is recommended due to ease of use

```bash
npm install -g @fenixengine/wizard

# Setup a new project
fenix

# Start the game server
fenix serve

# Build your plugins
fenix build

```

## Install With An Existing Project

If you want to include a local copy in an already existing project. This setup allows you to use a CI or other services to deploy and push your plugins.

```bash
npm install -D @fenixengine/wizard
```

Then you can add some helpful scripts to `package.json`:

```json
{
  "scripts": {
    "dev": "fenix serve",
    "build": "fenix build"
  }
}
```

Now you can start FeniXCLI server with:

```bash
npm run serve
```

And to build your plugins, run:

```bash
npm run build
```
By default all files are built to `game/js/plugins/`, which can be configured using CLI options.

## Setup A New Project

Before you can develop your plugins, you need to setup a brand new project. Nothing does that easier than `FeniXWizard`'s guided setup. To activate the project setup guide, run the `fenix` command with no arguments or run `fenix init`.

::: tip
For detailed information on the setup guide, take a look at the [CLI Options](../cli-options.html#init-command)
:::