# Parameters

Plugin parameters is the core file that RPG Maker MV utilizes so the game developer has a way to interact with the plugin. Without a parameters file, `FeniXWizard` cannot correctly build your plugin. `FeniXWizard` will automatically include the `Parameters.js` at the top of your final plugin bundle and you're not required to include it in your `main.js` file.

## New Tags
`FeniXWizard` utilizes new parameter tags, while a few are optional, others are not and will produce warnings or errors

```js
/*:
 * @plugindesc My plugin description
 * @author FeniXEngine Contributors
 *
 * New Tags -
 *
 * @pluginname MyPlugin
 * @modulename $myPluginGlobal
 * @external external-library
*/
```
## pluginname <Badge text="required"/>

 `@pluginname` This is the filename of the final bundle, which will be created to the default `'game/js/plugins/` directory.

## modulename <Badge text="optional"/>

:::warning
If exporting members to the global namespace, then not having this tag will produce a warning
:::

`@modulename` is used when you are creating a plugin that will be used by other plugin developers or you want to create a global namespace and expose important members. The module name will be the namespace created when bundling.

For example a modulename as `$myPluginGlobal` will result in a final bundle:

```js
var $myPluginGlobal = (function (exports) {
    // ModuleA...

    //ModuleB...

    //Exports
    exports.myPluginString = 'MyPlugin'
}
```

## external <Badge text="optional"/>

`@external` is used to let `FeniXWizard` know which external libraries are being used so it can exclude it from the final bundle. When bundling your plugins, you may freely use libraries and frameworks. If the library will be external, or provided by you for the plugin user to place into `js/libs/` then you need to explicitly let `FeniXWizard` know so it does not include it in the final bundle. Otherwise you will end up with a plugin file size way too large and loaded with a bunch of unnecessary code.

These are the packages included by default `'fs', 'http', 'https', 'path', 'PIXI'`