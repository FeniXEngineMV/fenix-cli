module.exports = {
  title: 'FeniXWizard',
  description: 'A lightweight, easy to use RPG Maker MV plugin development CLI',
  base: '/fenix-wizard/',
  dest: './public',
  head: [
    ['link', { rel: 'icon', type: "img/png", sizes: '180x180', href: `/img/favicons/apple-touch-icon.png` }],
    ['link', { rel: 'icon', type: "img/png", sizes: '32x32', href: `/img/favicons/favicon-32x32.png` }],
    ['link', { rel: 'icon', type: "img/png", sizes: '16x16', href: `/img/favicons/favicon-16x16.png` }]
  ],
  markdown: {
    activeHeaderLinks: true,
    anchor: { permalink: true }
  },
  themeConfig: {
    search: true,
    repo: 'https://gitlab.com/fenixenginemv/wizard/',
    repoLabel: 'Contribute!',
    docsDir: './docs/',
    sidebar: {
      '/guide/': [
        {
          collapsable: false,
          title: 'Guide',
          children: [
          '',
          'getting-started',
          'workflow-environment',
          'parameters',
          'bundler-plugin-system'
        ]
      }
      ]
    },
    nav: [
      { text: 'Guide', link: '/guide/' },
      { text: 'CLI Options', link: '/cli-options' },
      { text: 'Changelog', link: 'https://gitlab.com/FeniXEngineMV/fenix-cli/tags' },
    ]
  }
}
